window.addEventListener("load", function () {

    const tabs = document.querySelector(".tabs");
    tabs.addEventListener("click", tabsMenuHandler);

    function tabsMenuHandler(evt) {
        if (evt.target.tagName !== "LI"){
            return;
        }
        const allMenuBtns = evt.target.closest(".tabs").children;
        for (const btn of allMenuBtns) {
            if (btn !== evt.target) {
                btn.classList.remove("active");
            }
            else {
                evt.target.classList.add("active");
            }
        }
        const allTexts = document.querySelector(".tabs-content").children;
        for (const textElem of allTexts) {
            if (textElem.dataset.textId === evt.target.getAttribute("id")) {
                textElem.classList.add("active");
            }
            else {
                textElem.classList.remove("active");
            }
        }
    }
});
